﻿using System;
using ANTLRCalc.Gramatica;

namespace ANTLRCalc.Principal
{
	class Program
	{
		static void Main(string[] args)
		{
			while (true) {
				Console.Write("calc > ");
				string comando = Console.ReadLine();
				if (comando == "sair") {
					break;
				}
				Processador.Processar(comando);
			}
		}
	}
}