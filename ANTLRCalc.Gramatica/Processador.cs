﻿using Antlr.Runtime;
using Antlr.Runtime.Tree;

namespace ANTLRCalc.Gramatica
{
	public class Processador
	{
		private static CalcTree _treeParser;

		private Processador() { }

		public static void Processar(string comando)
		{
			// Somente na primeira inicialização o parâmetro ITreeNodeStream pode ser null
			// Por isso a variável foi definida como estática
			if (_treeParser == null) {
				_treeParser = new CalcTree(null);
			}

			// A string do comando é passada para o parser Lexer
			CalcLexer lexer = new CalcLexer(new ANTLRStringStream(comando));
			
			// Em seguida os tokens do lexer são passados para o Parser
			CalcParser tokenParser = new CalcParser(new CommonTokenStream(lexer));

			// E, por fim, a AST é passada para o parser Tree como uma CommonTree.
			// Devido a definição da opção 'ASTLabelType = CommonTree' na gramática Tree.
			// A chamada ao método calc() é pelo fato de 'calc' ser a regra mais abrangente da gramática
			CommonTree ast = (CommonTree)tokenParser.calc().Tree;
			if (ast == null) return;

			// ITreeNodeStream que é passado como null na inicialização é setado com a AST atualizada a cada nova execução de um comando
			_treeParser.SetTreeNodeStream(new CommonTreeNodeStream(ast));

			// A regra 'comando' é executada
			_treeParser.comando();
		}
	}
}