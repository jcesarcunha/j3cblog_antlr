﻿using System;
using System.Collections.Generic;

namespace ANTLRCalc.Gramatica
{
	public class Calculadora
	{
		private Dictionary<string, int> _variaveis;

		public Calculadora()
		{
			_variaveis = new Dictionary<string, int>();
		}

		public void AdicionarVariavel(string nome, int valor)
		{
			if (nome != string.Empty && valor >= 0) {
				if (_variaveis.ContainsKey(nome)) {
					_variaveis.Remove(nome);
				}

				_variaveis.Add(nome, valor);
			}
			else {
				throw new Exception("O nome e o valor da variável devem ser informados");
			}
		}

		public int Variavel(string nome)
		{
			if (nome != string.Empty) {
				if (_variaveis.ContainsKey(nome)) {
					return _variaveis[nome];
				}
				else {
					throw new Exception(string.Format("Variável {0} inexistente", nome));
				}
			}
			else {
				throw new Exception("Nome da varíavel não informado");
			}
		}

		public int Calcular(int valor1, string operacao, int valor2)
		{
			switch (operacao) {
				case "-": return valor1 - valor2;
				case "+": return valor1 + valor2;
				default: throw new Exception(string.Format("Operação {0} inválido", operacao));
			}
		}

		public int Calcular(string variavel, string operacao, int valor)
		{
			int v1 = Variavel(variavel);
			return Calcular(v1, operacao, valor);
		}

		public int Calcular(int valor, string operacao, string variavel)
		{
			int v2 = Variavel(variavel);
			return Calcular(valor, operacao, v2);
		}

		public int Calcular(string variavel1, string operacao, string variavel2)
		{
			int v1 = Variavel(variavel1);
			int v2 = Variavel(variavel2);
			return Calcular(v1, operacao, v2);
		}
	}
}